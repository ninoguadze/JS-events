document.addEventListener('DOMContentLoaded', function() {
    // Create the section element
    const section = document.createElement('section');
    section.className = 'app-section app-section--image-join';
  
    // Create the h2 element
    const h2 = document.createElement('h2');
    h2.className = 'app-title';
    h2.textContent = 'Join Our Program';
  
    // Create the h3 element
    const h3 = document.createElement('h3');
    h3.className = 'app-subtitle';
    h3.innerHTML = 'Sed do eiusmod tempor incididunt <br> ut labore et dolore magna aliqua.';
  
    // Create the form element
    const form = document.createElement('form');
    form.className = "join-form"
    form.addEventListener('submit', function(event) {
      event.preventDefault(); // Prevent the default form submission behavior
  
      const emailInput = event.target.elements['email'];
      const enteredValue = emailInput.value;
      console.log('Entered value:', enteredValue);
    });
  
    // Create the input element
    const input = document.createElement('input');
    input.type = 'email';
    input.name = 'email'; // Set the name attribute to easily access it in the form submission handler
    input.className = 'app-section__input app-section__input--email';
    input.placeholder = 'Email';
  
    // Create the button element
    const button = document.createElement('button');
    button.className = 'app-section__button app-section__button--subscribe';
    button.textContent = 'subscribe';
  
    // Append the elements to the form
    form.appendChild(input);
    form.appendChild(button);
  
    // Append the elements to the section
    section.appendChild(h2);
    section.appendChild(h3);
    section.appendChild(form);
  
    // Append the section to the document body (or any desired parent element)
    document.getElementsByClassName("app-section app-section--image-culture")[0].after(section)  
  });
  